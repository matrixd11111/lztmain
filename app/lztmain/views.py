from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.generic import ListView
from django.db.models import Q

from .models import *
from .forms import *


def main(request):
    context = {
        'title': 'Главная'
    }
    return render(request, 'lztmain/index.html', context)

def clients(request):
    clients = Client.objects.all()
    context = {
        'title': 'Клиенты',
        'clients': clients,
    }
    return render(request, 'lztmain/clients.html', context)

def clients_add(request):
    form = ClientForm()
    if request.method == 'POST':
        form = ClientForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/')
    context = {
        'title': 'Добавление клиента',
        'form': form,
    }
    return render(request, 'lztmain/clients_add.html', context)

def stats_sample(request):

    performer_id = request.POST.get('order_performer')
    start_date = request.POST.get('start_date')
    end_date = request.POST.get('end_date')

    if performer_id:
        #Все заказы определенного исполнителя за определенный промежуток
        orders_performer = Orders.objects.filter(
            date_create__range=(start_date, end_date),
            order_performer=performer_id,
        )
    
    service_all = Service.objects.filter(
        date_create__range=(start_date, end_date),
    )
    x = service_all.annotate(service_name)
    print(dir(x))
    print(x)
    
    order_form = OrdersForm()
    
    context = {
        'title': 'Статистика выборка',
        'order_form': order_form,
        'x': x,
    }
    return render(request, 'lztmain/stats_sample.html', context)


def orders_history(request):
    context = {
        'title': 'История заказов'
    }
    return render(request, 'lztmain/orders_history.html', context)


def orders_add(request):
    form = OrdersForm()

    if request.method == 'POST':
        form = OrdersForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/')
    context = {
        'title': 'Добавление заказа',
        'form': form,
    }
    return render(request, 'lztmain/orders_add.html', context)


def orders_statistics(request):
    context = {
        'title': 'Статистика заказов'
    }
    return render(request, 'lztmain/orders_statistics.html', context)


def services(request):
    context = {
        'title': 'Услуги'
    }
    return render(request, 'lztmain/services.html', context)


def services_add(request):
    form = ServiceForm()

    if request.method == 'POST':
        form = ServiceForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/')
    context = {
        'title': 'Добавление услуги',
        'form': form
    }
    return render(request, 'lztmain/services_add.html', context)


def services_statistics(request):
    context = {
        'title': 'Статистика услуг'
    }
    return render(request, 'lztmain/services_statistics.html', context)


def stats(request):
    context = {
        'title': 'Статистика'
    }
    return render(request, 'lztmain/stats.html', context)


class SearchResultsView(ListView):
    model = Client
    template_name = 'lztmain/search_results.html'

    def get_queryset(self):
        query = self.request.GET.get('q')
        object_list = Client.objects.filter(
            Q(name__icontains=query) | Q(surname__icontains=query)
        )
        return object_list
