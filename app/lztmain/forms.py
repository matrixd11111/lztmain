from django import forms

from .models import ( Client,
                      Orders,
                      Service,
                      ServiceOrders,
)


class ClientForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = [
            'name',
            'surname',
            'third_name',
            'birth_date',
            'phone_number',
            'note',
            'email',
            'patient',
            'client_type',
        ]
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for fields_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'


class OrdersForm(forms.ModelForm):
    class Meta:
        model = Orders
        fields = [
            'order_date',
            'order_service',
            'order_performer',
            'order_note',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for fields_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'
            field.help_text = ''


class ServiceForm(forms.ModelForm):
    class Meta:
        model = Service
        fields = [
            'service_name',
            'service_cod',
            'service_note',
            'service_expenses',
            'service_price',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for fields_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'
            field.help_text = ''


class ServiceOrdersForm(forms.ModelForm):
    class Meta:
        model = ServiceOrders
        fields = [
            'service_of_orders',
            'service_cost_of_orders',
        ]
